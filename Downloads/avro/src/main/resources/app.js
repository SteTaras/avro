const express = require('express');
const cassandra = require('cassandra-driver');
const path = require('path');
const async = require('async');
const bodyParser = require('body-parser');

const app = express();
//Connect to the cluster
const cassandraClient = new cassandra.Client({
    contactPoints: ['127.0.0.1'],
    keyspace: 'taxi'});

const port = 3000;

const router = express.Router();

var currTime = 0;


router.get('/api/getDataForSec', function(req, res) {
    currTime += 1;
    const qw = "select * from taxi_drive where group_id = 1";
    console.log(qw);

    cassandraClient.execute(qw, function(err, result) {


        let models = [];
        for(let i = 0; i < result.rows.length; i++) {
            const row = result.rows[i];
            models[i] = row;
        }


        res.setHeader('Access-Control-Allow-Origin','*');
        res.json(models);

    });
});


app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    limit: '100mb',
    extended: true
}));

app.use(express.static(__dirname + '/ui'));


router.get('/', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.sendFile(path.join(__dirname + '/ui/index.html'));
});

app.use(router);
app.listen(port);